import { Component, Input } from '@angular/core';

@Component({
    selector: 'table-component',
    templateUrl: './table.component.html'
})

export class TableComponent
{
    _headers = [];

    _rows = [];

    @Input() set headers (headers: any)
    {
        this._headers = headers ? headers : [];

        this.checkHeaders();
    }

    @Input() set rows (rows: any)
    {
        this._rows = rows ? rows : [];

        this.checkHeaders();
    }

    constructor () { }

    checkHeaders ()
    {
        if (this._headers.length > 0)
        {
            return true;
        }


        if (this._rows.length > 0)
        {
            let row = this._rows[0];

            this._headers = [];

            for (let property in row)
            {
                this._headers.push(property);
            }

            return true;
        }

        return false;
    }
}
