import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { ThingsComponent } from '../things.component';

@Injectable()

export class UnsavedChangesGuard implements CanDeactivate<ThingsComponent>
{
    canDeactivate (component: any)
    {
        return component.askCancel();
    }
}
