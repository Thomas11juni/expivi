import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { ThingComponent } from './thing.component';

@Component({
    selector: 'things-component',
    templateUrl: './things.component.html'
})

export class ThingsComponent implements OnInit
{ 
    things = [];

    thingsFiltered = [];

    query = '';

    detailEditId = null;

    @ViewChild(ThingComponent) thingComponent: ThingComponent;

    constructor (
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router
    )
    {
        // Route params is empty so I chose to go with this solution
        router.events.subscribe((event) => {
            if (event instanceof NavigationEnd)
            {
                if (router.url == '/')
                {
                    if (this.detailEditId)
                    {
                        this.getThings();
                    }

                    this.detailEditId = null;

                    return;
                }

                let segments = router.url.split('/');

                if (segments[1] == 'edit')
                {
                    this.detailEditId = segments[2];
                }
            }
        });
    }

    askCancel ()
    {
        return this.thingComponent.askCancel();
    }

    ngOnInit ()
    {
        this.http.get('http://expivi-api.1106.nl/things').subscribe(data => {
            this.things = data['things'];

            this.thingsFiltered = this.things;
        });
    }

    clearQuery ()
    {
        this.query = '';

        this.search();
    }

    getThings ()
    {
        this.http.get('http://expivi-api.1106.nl/things').subscribe(data => {
            this.things = data['things'];

            this.thingsFiltered = this.things;
        });
    }

    search ()
    {
        if (!this.query)
        {
            this.thingsFiltered = this.things;

            return true;
        }


        let searchProperties = ['value_1', 'value_2', 'value_3'];

        this.thingsFiltered = this.things.filter(thing => {
            let contains = false;

            for (let property of searchProperties)
            {
                if (thing[property] && thing[property].indexOf(this.query) >= 0)
                {
                    contains = true;

                    break;
                }
            }

            return contains;
        });
    }
}
