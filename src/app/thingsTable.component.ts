import { Component, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TableComponent } from './table.component';

@Component({
    selector: 'things-table-component',
    templateUrl: './thingsTable.component.html'
})

export class ThingsTableComponent extends TableComponent
{
    editingIndex = -1;

    notEditable = ['id', 'created_at', 'updated_at'];

    constructor (private http: HttpClient)
    {
        super();
    }

    cancelEdit ()
    {
        this.editingIndex = -1;
    }

    editRow (index: number)
    {
        this.editingIndex = index;
    }

    saveRow (index: number)
    {
        let row = this._rows[index];

        if (!row) return false;


        let url = 'http://expivi-api.1106.nl/thing/' + row.id + '/update';

        this.http.post(url, row).subscribe(data => {
            this.cancelEdit();
        });
    }
}
