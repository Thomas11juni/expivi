import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location  } from '@angular/common';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'thing-component',
    templateUrl: './thing.component.html',
    styleUrls: ['./thing.component.css']
})

export class ThingComponent
{

    _id = null;

    fields = [
        'id',
        'value_1',
        'value_2',
        'value_3',
        'secret_value_1',
        'secret_value_2',
        'created_at',
        'updated_at'
    ];

    notEditable = ['id', 'created_at', 'updated_at'];

    thing = null;

    skipAskCancel = false;

    @Input() set id (id: number)
    {
        this._id = id;

        this.getThing();
    }

    @ViewChild('thingForm') thingForm: any;

    constructor (
        private http: HttpClient,
        private router: Router
    ) {}

    askCancel ()
    {
        if (!this.skipAskCancel && this.thingForm.dirty)
        {
            return confirm('Unsaved changes have been detected! Do you wish to continue?');
        }

        return true;
    }

    cancel ()
    {
        this.router.navigateByUrl('/');
    }

    getThing ()
    {
        if (this._id)
        {
            let url = 'http://expivi-api.1106.nl/thing/' + this._id;

            this.http.get(url).subscribe(data => {
                this.thing = data['thing'];
            });
        }
    }

    save ()
    {
        let url = 'http://expivi-api.1106.nl/thing/' + this.thing.id + '/update';

        this.http.post(url, this.thing).subscribe(data => {
            this.skipAskCancel = true;

            this.cancel();
        });        
    }
}
