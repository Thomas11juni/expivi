import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { UnsavedChangesGuard } from './guards/unsavedChanges.guard';

import { AppComponent } from './app.component';
import { TableComponent } from './table.component';
import { ThingComponent } from './thing.component';
import { ThingsComponent } from './things.component';
import { ThingsTableComponent } from './thingsTable.component';

@NgModule({
    declarations: [
        AppComponent,
        TableComponent,
        ThingComponent,
        ThingsComponent,
        ThingsTableComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot([
            {
                path: '',
                component: ThingsComponent
                // Children give null as compnent in guard
                /*,
                canDeactivate: [ UnsavedChangesGuard ],
                children: [{
                    path: 'edit/:id',
                    component: ThingsComponent,
                    canDeactivate: [ UnsavedChangesGuard ]
                }]*/
            },
            {
                path: 'edit/:id',
                component: ThingsComponent,
                canDeactivate: [ UnsavedChangesGuard ]
            }
        ])
    ],
    providers: [ UnsavedChangesGuard ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
